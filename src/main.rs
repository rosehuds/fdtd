extern crate device_tree;
extern crate syscall;

use std::fs::File;
use std::io::{Read, Write};

use syscall::data::{TimeSpec, Packet};
use syscall::scheme::SchemeMut;

mod scheme;

fn main() {
    if unsafe { syscall::clone(0).unwrap() } == 0 {
        let mut buffer = vec![];
        let mut dtb_file = File::open("sys:dtb").expect("fdtd: failed to open sys:dtb");
        dtb_file.read_to_end(&mut buffer).expect("fdtd: failed to read sys:dtb");
        let device_tree = device_tree::DeviceTree::load(&buffer).expect("fdtd: failed to parse dtb");

        let mut time_file = File::open("time:4").expect("fdtd: failed to open time:");
        let mut start_time = TimeSpec::default();
        time_file.read_exact(&mut start_time).expect("fdtd: failed to get time");

        let mut socket = File::create(":fdt").expect("fdtd: failed to create scheme");
        let mut scheme = scheme::DeviceTreeScheme::new(device_tree, start_time);

        syscall::setrens(0, 0).expect("fdtd: failed to enter null namespace");

        loop {
            let mut packet = Packet::default();
            socket.read(&mut packet).expect("fdtd: failed to read events from scheme");

            scheme.handle(&mut packet);

            socket.write(&packet).expect("fdtd: failed to write responses to scheme");
        }
    }
}
