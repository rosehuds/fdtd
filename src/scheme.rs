use std;
use std::collections::BTreeMap;

use device_tree::DeviceTree;
use syscall::{
    SchemeMut,
    Error,
    error::*,
    data::{Stat, TimeSpec},
    flag::{MODE_FILE, MODE_DIR},
};

pub struct DeviceTreeScheme {
    device_tree: DeviceTree,
    start_time: TimeSpec,
    file_paths: BTreeMap<usize, (String, Option<String>)>,
    next_id: usize,
}

impl DeviceTreeScheme {
    pub fn new(device_tree: DeviceTree, start_time: TimeSpec) -> Self {
        DeviceTreeScheme {
            device_tree,
            start_time,
            file_paths: BTreeMap::new(),
            next_id: 0,
        }
    }
}

impl SchemeMut for DeviceTreeScheme {
    fn open(&mut self, path: &[u8], flags: usize, uid: u32, gid: u32) -> Result<usize> {
        let path = std::str::from_utf8(path).map_err(|_| Error::new(EINVAL))?;
        let (node_path, maybe_prop) = match self.device_tree.find(path) {
            Some(_) => {
                (path.to_owned(), None)
            },
            None => {
                let node_path = path.trim_right_matches(|c| c != '/');
                match self.device_tree.find(node_path) {
                    Some(node) => {
                        let prop_name = &path[node_path.len()..];
                        if !node.has_prop(prop_name) {
                            return Err(Error::new(ENOENT));
                        }
                        (node_path.to_owned(), Some(prop_name.to_owned()))
                    },
                    None => {
                        return Err(Error::new(ENOENT));
                    }
                }
            },
        };
        node_path.trim_right_matches('/');

        let id = self.next_id;
        self.next_id += 1;
        self.file_paths.insert(id, (node_path, maybe_prop));
        return Ok(id);
    }

    fn read(&mut self, id: usize, buf: &mut [u8]) -> Result<usize> {
        let (node_path, maybe_prop) = self.file_paths.get(&id).ok_or(Error::new(EBADF))?;
        let node = self.device_tree.find(node_path).unwrap();
        match maybe_prop {
            Some(prop_name) => {
                let data = node.prop_raw(prop_name).unwrap();
                let mut bytes = 0;
                for (src, dest) in data.iter().zip(buf) {
                    *dest = *src;
                    bytes += 1;
                }
                return Ok(bytes);
            },
            None => {
                let mut i = 0;
                for prop in node.props.iter() {
                    for byte in prop.0.as_bytes() {
                        buf[i] = *byte;
                        i += 1;
                    }
                    buf[i] = b'\n';
                    i += 1;
                }
                return Ok(i);
            }
        }
    }

    fn fstat(&mut self, id: usize, stat: &mut Stat) -> Result<usize> {
        let (node_path, maybe_prop) = self.file_paths.get(&id).ok_or(Error::new(EBADF))?;
        let TimeSpec { tv_sec, tv_nsec } = self.start_time;
        let (sec, nsec) = (tv_sec as u64, tv_nsec as u32);

        let node = self.device_tree.find(node_path).unwrap();
        let (mode, size) = match maybe_prop {
            Some(prop) => {
                (MODE_FILE, node.prop_raw(prop).unwrap().len())
            },
            None => {
                let mut size = 0;
                for prop in node.props.iter() {
                    size += prop.0.len() + 1;   // \n
                }
                (MODE_DIR, size)
            }
        };
        *stat = Stat {
            st_dev: 0,
            st_ino: 0,
            st_mode: mode,
            st_nlink: 1,
            st_uid: 0,
            st_gid: 0,
            st_size: size as u64,
            st_blksize: 0,
            st_blocks: 0,
            st_mtime: sec,
            st_mtime_nsec: nsec,
            st_atime: sec,  // TODO: access time?
            st_atime_nsec: nsec,
            st_ctime: sec,
            st_ctime_nsec: nsec,
        };
        Ok(0)
    }
}
